#!/usr/bin/perl

# Run test harness using .test files from the test directory.
#
# Copyright (c) 2014, 2015 Brendan O'Dea <bod@debian.org>
# All rights reserved.
#
# Use of this source code is governed by a BSD-style license that can be
# found in the COPYING file or at http://opensource.org/licenses/BSD-3-Clause
#
# Patterns may be specified to limit the tests to just those matching the
# patterns.  See the README file in the "tests" directory for the test file
# format.
#
# The --verbose option may be specified more than once to provide more detail.

use strict;
use warnings;

(my $cmd = $0) =~ s#.*/##;
my $WIDTH = 30;  # max width of test names
my $USAGE = <<EOT;
Usage: $cmd [-v|--verbose] [-V|--valgrind] directory [pattern ...]
EOT

my $verbose = 0;
my $valgrind = 0;
while (@ARGV and $ARGV[0] =~ /^-/) {
  $_ = shift;
  if (/^(-v|--verbose)$/) {
    $verbose++;
    $|++;
  } elsif (/^(-V|--valgrind)$/) {
    $valgrind++;
  } elsif (!/^--$/) {
    die $USAGE;
  }
}

my $testdir = shift or die $USAGE;
my @tests = glob "$testdir/*.test" or die "$cmd: no tests found in $testdir\n";
my @filters = map qr/$_/, @ARGV;
my $status = 0;
my $tests_run = 0;
my $tests_passed = 0;

for my $test (@tests) {
  next if @filters and not grep $test =~ $_, @filters;

  (my $test_name = $test) =~ s/\.test$//;
  for ($test_name) {
    s#.*/##;
    s/^\d+_//;
    tr/_/ /;
  }

  open my $t, $test or die "$cmd: can't open $test ($!)\n";

  my @args;
  my @expect;
  my $target;
  while (<$t>) {
    next if /^\s*#/;  # skip comments
    s/^\s+//;  # remove leading spaces
    s/\s+$//;  # remove trailing spaces
    next unless length;
    if (/^\[args\]$/) {
      $target = \@args;
      next;
    } elsif (/^\[expect\]$/) {
      $target = \@expect;
      next;
    } else {
      die "$cmd: test file $test requires [args] and [expect] headings\n"
          unless $target;
    }
    push @$target, $_;
  }

  @expect = map qr/$_/, @expect;  # compile matches
  $tests_run++;

  if ($verbose) {
    my $ndots = $WIDTH - length $test_name;
    my $dots = $ndots > 0 ? '.' x $ndots : '';
    printf "%2d. \u$test_name$dots...", $tests_run;
  }

  my @cmd = ('./nls_search_test', @args);
  if ($valgrind) {
    # 10 = valgrind error
    unshift @cmd, 'valgrind', '--leak-check=full', '--error-exitcode=10';
  }

  open my $harness, '-|', @cmd or die "$cmd: can't run @cmd ($!)\n";
  my @debug_output;
  while (<$harness>) {
    chomp(my $cmd_output = $_);
    @expect = grep !($cmd_output =~ $_), @expect;
    push @debug_output, $cmd_output if $verbose > 1;
  }

  close $harness or die "$cmd: error closing @cmd ($!)\n";
  if ($? or @expect) {
    $status = $? ? 8 : 9;  # 8 = test execution failed, 9 = output missmatch
    if ($verbose) {
      print "FAIL\n";
    } else {
      warn "$cmd: test $test failed\n";
    }
  } else {
    print "PASS\n" if $verbose;
    $tests_passed++;
  }

  if ($verbose > 1) {
    if (@debug_output) {
      print join "\n  »", '  Output:', @debug_output;
      print "\n";
    } else {
      print "  No output.\n";
    }
    if (@expect) {
      print join "\n  »", '  Unmatched:', @expect;
      print "\n";
    }
  }
}

print "Passed $tests_passed of $tests_run tests.\n" if $verbose;
exit $status;
