/*
 * Find the path which best matches the user's language preferences.
 *
 * Copyright (c) 2014, 2015 Brendan O'Dea <bod@debian.org>
 * All rights reserved.
 *
 * Use of this source code is governed by a BSD-style license that can be
 * found in the COPYING file or at http://opensource.org/licenses/BSD-3-Clause
 *
 * Preferences are taken from the environment variables LANGUAGE, LC_ALL,
 * LC_MESSAGES and LANG.  Refer to the gettext documentation for more details
 * as to how these are interpreted.
 *
 * Example:
 *
 *   struct nls_search_prefs *prefs = nls_search_get_prefs();
 *   char *dir_n = nls_search_find_file("/usr/share/info/%s/dir", prefs);
 *   char *dir = dir_n ? dir_n : "/usr/share/info/dir";
 *   // do something with dir
 *   free(dir_n);
 *   nls_search_free_prefs(prefs);
 */

#include <stddef.h>

enum nls_search_type {
  NLS_SEARCH_ANY,
  NLS_SEARCH_FILE,
  NLS_SEARCH_DIRECTORY,
};

/* Linked list of preferred languages, broken into pieces. */
struct nls_search_prefs {
  /* The language field is typically copied from the environment, the
     delimiters replaced with '\0' and the territory, codeset and modifier
     fields refer to that copy. */
  char *language;  /* allocated */
  char *territory;
  char *codeset;
  char *modifier;
  /* When territory is omitted in $LANGUAGE, the default derived from language
     may be used.  Only one of territory or default_territory will be set (a
     separate field is used only to track allocation). */
  char *default_territory;  /* allocated */
  /* Copy of codeset, normalised in the same way as gettext. */
  char *normalised_codeset;  /* allocated */
  struct nls_search_prefs *next;
};

/* Search for the first path which matches the template for the given language
   preferences.  The template should contain a %s token, which will be
   replaced by a string for the first matching language.  The result returned
   with be an allocated pointer which must be released by the caller with
   free(), or NULL if no matching path was found.  If the template doesn't
   include a token, then a trailing "%s" is implied. */
#define nls_search(template, prefs, type) \
  nls_search_with_buf(template, prefs, type, NULL, NULL);

#define nls_search_file(template, prefs) \
  nls_search(template, prefs, NLS_SEARCH_FILE);

#define nls_search_directory(template, prefs) \
  nls_search(template, prefs, NLS_SEARCH_DIRECTORY)

/* Variant which may be provided a buffer to write the path to, rather that
   creating one with malloc.  The length of the buffer must be passed with the
   size pointer.  If there is insufficient space in the buffer, the size will
   be set to the required amount and NULL returned.  The caller may
   distinguish this NULL return from the one indicating no match by checking
   if this value has changed.  On successful match, size will be set to the
   length of the matching path. */
char *nls_search_with_buf(const char *template,
                          const struct nls_search_prefs *prefs,
                          enum nls_search_type type, char *buf, size_t *size);

/* Parse environment to find the user's language preferences.  The value
   returned is allocated, and should be released by nls_search_free_prefs(). */
struct nls_search_prefs *nls_search_get_prefs(void);
void nls_search_free_prefs(struct nls_search_prefs *prefs);

/* If set, this function will be called when malloc fails.  The size of the
   failing request and the code location are passed as arguments. */
extern void (*nls_search_memory_error_handler)(size_t size, const char *file,
                                               int line);
