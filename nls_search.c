/*
 * Find the path which best matches the user's language preferences.
 *
 * Copyright (c) 2014, 2015 Brendan O'Dea <bod@debian.org>
 * All rights reserved.
 *
 * Use of this source code is governed by a BSD-style license that can be
 * found in the COPYING file or at http://opensource.org/licenses/BSD-3-Clause
 */

#include "nls_search.h"

#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <locale.h>
#include <sys/types.h>
#include <sys/stat.h>

/* Memory error hook. */
void (*nls_search_memory_error_handler)(size_t size, const char *file,
                                        int line) = NULL;

/* Allow test harness to provide alternate implementations for some library
   calls.  These should typically not be set in user code. */
void *(*nls_search_malloc_)(size_t size) = malloc;
char *(*nls_search_strdup_)(const char *str) = strdup;
char *(*nls_search_setlocale_)(int category, const char *locale) = setlocale;
char *(*nls_search_getenv_)(const char *name) = getenv;
int (*nls_search_stat_)(const char *path, struct stat *buf) = stat;

static void *_malloc_wrapper(size_t size, const char *file, int line) {
  void *p = nls_search_malloc_(size);
  if (!p && nls_search_memory_error_handler) {
    nls_search_memory_error_handler(size, file, line);
  }
  return p;
}

static void *_strdup_wrapper(const char *str, const char *file, int line) {
  char *s = nls_search_strdup_(str);
  if (!s && nls_search_memory_error_handler) {
    nls_search_memory_error_handler(strlen(str), file, line);
  }
  return s;
}

#define MALLOC(size) _malloc_wrapper(size, __FILE__, __LINE__)
#define STRDUP(str) _strdup_wrapper(str, __FILE__, __LINE__)
#define SETLOCALE nls_search_setlocale_
#define GETENV nls_search_getenv_
#define STAT nls_search_stat_

/* Free a list of prefs. */
void nls_search_free_prefs(struct nls_search_prefs *prefs) {
  struct nls_search_prefs *tmp;
  while (prefs) {
    free(prefs->language);
    free(prefs->default_territory);
    free(prefs->normalised_codeset);
    tmp = prefs;
    prefs = prefs->next;
    free(tmp);
  }
}

/* Split a locale name and return an allocated nls_search_prefs struct. */
static struct nls_search_prefs *_split_locale(const char *name,
                                              char default_territory) {
  char *p;
  struct nls_search_prefs *new;
  if (!(new = MALLOC(sizeof(*new)))) {
    return NULL;
  }

  memset(new, 0, sizeof(*new));
  if (!(new->language = STRDUP(name))) {
    nls_search_free_prefs(new);
    return NULL;
  }

  /* language[_territory[.codeset]][@modifier] */
  for (p = new->language; *p; p++) {
    if (*p == '_' && !new->territory) {
      new->territory = p + 1;
    } else if (*p == '.' && !new->codeset) {
      new->codeset = p + 1;
    } else if (*p == '@' && !new->modifier) {
        new->modifier = p + 1;
    } else {
      continue;
    }
    *p = '\0';
  }

  /* Check for empty parts, which can occur with consecutive delimiters. */
  if (!*new->language) {
    /* Unlikely to be valid.  Set to the original value and hope. */
    memset(new, 0, sizeof(*new));
    strcpy(new->language, name);
    return new;
  }

  if (new->territory && !new->territory) new->territory = NULL;
  if (new->codeset && !new->codeset)     new->codeset = NULL;
  if (new->modifier && !new->modifier)   new->modifier = NULL;

  if (!new->territory && default_territory) {
    new->default_territory = STRDUP(new->language);
    for (p = new->default_territory; *p; p++) {
      *p = toupper(*p);
    }
  }

  /* Create a normalised codeset:
     - drop non alpha-numeric characters
     - fold case
     - prefix result with "iso" if all numeric */
  if (new->codeset) {
    char all_numeric = 1;
    char *q;

    /* Allocate enough space for the whole string, plus three for "iso". */
    if (!(new->normalised_codeset = MALLOC(strlen(new->codeset) + 4))) {
      nls_search_free_prefs(new);
      return NULL;
    }

    for (p = new->codeset, q = new->normalised_codeset; *p; p++) {
      if (isdigit(*p)) {
        *q++ = *p;
      } else if (isalpha(*p)) {
        *q++ = tolower(*p);
        all_numeric = 0;
      }
    }
    *q = '\0';

    if (!*new->normalised_codeset ||
        !strcmp(new->normalised_codeset, new->codeset)) {
      free(new->normalised_codeset);
      new->normalised_codeset = NULL;
    } else if (all_numeric) {
      q = new->normalised_codeset;
      memmove(q + 3, q, strlen(q) + 1);
      memcpy(q, "iso", 3);
    }
  }

  return new;
}

/* Parse environment to find the user's language preferences. */
struct nls_search_prefs *nls_search_get_prefs() {
  struct nls_search_prefs *prefs = NULL;

  char *env;
  /* Determine locale as defined by LC_ALL, LC_MESSAGES or LANG. */
  char *locale = SETLOCALE(LC_MESSAGES, "");
  if (locale) {
    /* A message locale of "C" is interpreted as meaning that no localisation
       should occur. */
    if (!strcmp(locale, "C")) {
      return NULL;
    }

    prefs = _split_locale(locale, 0);
  }

  /* LANGUAGE is a colon-delimited list of choices. */
  if ((env = GETENV("LANGUAGE"))) {
    char *delim;
    char *env_copy = STRDUP(env);  /* make a copy */
    if (!env_copy) {
      nls_search_free_prefs(prefs);
      return NULL;
    }

    /* Split the list, pushing preferences onto the start of the list from
       right to left. */
    do {
      delim = strrchr(env_copy, ':');
      if (delim) {
        *delim++ = '\0';
        locale = delim;
      } else {
        locale = env_copy;
      }

      if (*locale) {
        struct nls_search_prefs *new = _split_locale(locale, 1);
        if (!new) {
          nls_search_free_prefs(prefs);
          return NULL;
        }
        new->next = prefs;
        prefs = new;
      }
    } while (delim);

    free(env_copy);
  }

  return prefs;
}

/* Lookup a path for a single language preference, inserting the locale string
   between the left and right parts. */
static char *_exists_path(const char *left, size_t left_len,
                          const char *right, size_t right_len,
                          const struct nls_search_prefs *pref,
                          enum nls_search_type type, char *buf, size_t *size) {
# define USE_LANGUAGE           0
# define USE_TERRITORY          1  /* territory or default_territory */
# define USE_CODESET            2
# define USE_NORMALISED_CODESET 4
# define USE_MODIFIER           8
  char try = USE_LANGUAGE;
  char last_try;
  char *path;
  char *mid;

  /* Initial length will be the size of the path component before the locale
     string, the component after, and the \0 terminator */
  size_t len = left_len + right_len + 1;
  /* Add the locale components (each plus one for the delimiter). */
  len += strlen(pref->language);
  if (pref->territory) {
    len += strlen(pref->territory) + 1;
    try |= USE_TERRITORY;
  }

  if (pref->default_territory) {
    len += strlen(pref->default_territory) + 1;
    try |= USE_TERRITORY;
  }

  if (pref->codeset) {
    size_t c = strlen(pref->codeset);
    if (pref->normalised_codeset) {
      size_t d = strlen(pref->normalised_codeset);
      if (d > c) {
        c = d;
      }
      try |= USE_NORMALISED_CODESET;
    }
    len += c + 1;
    try |= USE_CODESET;
  }

  if (pref->modifier) {
    len += strlen(pref->modifier) + 1;
    try |= USE_MODIFIER;
  }

  /* Use buffer if provided */
  if (buf) {
    if (len > *size) {
      *size = len;
      return NULL;
    }
    path = buf;
  } else {
    if (!(path = MALLOC(len))) {
      return NULL;
    }
  }

  *path = '\0';
  if (left_len) {
    strncat(path, left, left_len);
  }

  strcat(path, pref->language);

  /* Keep a pointer to the end of the language string, to make appending
     different locale strings simpler. */
  mid = path + left_len + strlen(pref->language);

  do {
    struct stat st_buf;
    char *p = mid;
    *p = '\0';

    if (try & USE_TERRITORY) {
      size_t len;
      *p++ = '_';
      strcpy(p, pref->territory ? pref->territory : pref->default_territory);
      len = strlen(p);
      p += len;
    }

    if (try & USE_CODESET) {
      *p++ = '.';
      strcpy(p, pref->codeset);
      p += strlen(pref->codeset);
    } else if (try & USE_NORMALISED_CODESET) {
      *p++ = '.';
      strcpy(p, pref->normalised_codeset);
      p += strlen(pref->normalised_codeset);
    }

    if (try & USE_MODIFIER) {
      *p++ = '@';
      strcpy(p, pref->modifier);
      p += strlen(pref->modifier);
    }

    if (right_len) {
      strncat(p, right, right_len);
    }

    if (!STAT(path, &st_buf)) {
      switch (type) {
      case NLS_SEARCH_FILE:
        if (S_ISREG(st_buf.st_mode)) return path;
        break;
      case NLS_SEARCH_DIRECTORY:
        if (S_ISDIR(st_buf.st_mode)) return path;
        break;
      default: /* NLS_SEARCH_ANY */
        return path;
      }
    }

    last_try = try;
    if (try & USE_CODESET) {
      try &= ~USE_CODESET;
    } else if (try & USE_NORMALISED_CODESET) {
      try &= ~USE_NORMALISED_CODESET;
    } else if (try & USE_TERRITORY) {
      try &= ~USE_TERRITORY;
    } else if (try & USE_MODIFIER) {
      try &= ~USE_MODIFIER;
    }
  } while (try != last_try);

  if (!buf) free(path);
  return NULL;
}

/* Locate the first path matching the template for the language
   preferences. */
char *nls_search_with_buf(const char *template,
                          const struct nls_search_prefs *prefs,
                          enum nls_search_type type, char *buf, size_t *size) {
  const char *left = NULL;
  const char *right = NULL;
  size_t left_len = 0;
  size_t right_len = 0;
  char *token = strstr(template, "%s");
  if (token) {
    if (token != template) {
      left = template;
      left_len = token - template;
    }
    if (*(token + 2)) {
      right = token + 2;
      right_len = strlen(right);
    }
  } else {
    /* No token, trailing implied. */
    left = template;
    left_len = strlen(template);
  }

  while (prefs) {
    char *match = _exists_path(left, left_len, right, right_len, prefs, type,
                               buf, size);
    if (match) {
      if (size) {
        *size = strlen(match);
      }
      return match;
    }

    prefs = prefs->next;
  }

  return NULL;
}
