.\" Copyright (c) 2014, 2015 Brendan O'Dea <bod@debian.org>
.\" All rights reserved.
.\"
.\" Use of this source code is governed by a BSD-style license that can be
.\" found in the COPYING file or at http://opensource.org/licenses/BSD-3-Clause
.TH NLS_SEARCH 3 "February 2015"
.SH NAME
nls_search, nls_search_file, nls_search_directory, nls_search_with_buf,
nls_search_get_prefs, nls_search_free_prefs \- find a path best suiting the
user's language preferences
.SH SYNOPSIS
.B
#include <nls_search.h>
.LP
.PD 0
.HP
.BI "char *nls_search(const char *" template \
", const struct nls_search_prefs *" prefs \
", enum nls_search_type " type ");"
.HP
.BI "char *nls_search_file(const char *" template \
", const struct nls_serch_prefs *" prefs ");"
.HP
.BI "char *nls_search_directory(const char *" template \
", const struct nls_serch_prefs *" prefs ");"
.HP
.BI "char *nls_search_with_buf(const char *" template \
", const struct nls_search_prefs *" prefs \
", enum nls_search_type " type ", char *" buf ", size_t *" size ");"
.HP
.B struct nls_search_prefs *nls_search_get_prefs(void);
.HP
.BI "void nls_search_free_prefs(struct nls_search_prefs *" prefs ");"
.PD
.HP
.BI "extern void (*nls_search_memory_error_handler)(size_t " \
size ", const char *" file ", int " line ");"
.SH DESCRIPTION
The
.B nls_search
library provides a set of functions to find a path which best suits the user's
language preferences.  This selection process is intended to duplicate that
used by
.BR gettext (3) .
.PP
The basic function is
.B nls_search
which searches for a path which matches the given
.I template
to the language preferences and type specified by
.I prefs
and
.IR type .
The template string must include a
.B %s
token which will be replaced by the best language string found.  Preferences
.RI ( prefs )
are typically derived from the environment using the
.B nls_search_get_prefs
function (see example below) and the required type should be selected as one
of the enumeration values
.BR NLS_SEARCH_ANY ,
.BR NLS_SEARCH_FILE ,
or
.BR NLS_SEARCH_DIRECTORY .
.PP
Convenience functions (actually macros)
.B nls_search_file
and
.B nls_search_directory
are provided to simplify the common cases of identifying a match for a file or
directory and call
.B nls_search
passing the appropriate value for the
.I type
argument.
.PP
The
.BR nls_search ,
.B nls_search_file
and
.B nls_search_directory
functions when successful return a value allocated by malloc.  Special
handling of malloc failures may be provided by setting
.B nls_search_memory_error_handler
to a function which takes three arguments: the size of the failed request, and
the file and line number where the failure occured.  There is no provision for
recovering from such errors: this handler is provided so that the failure can
be reported to the user in a way that suits the application.  Such malloc
failures will manifest as unable to find a match for the given locale-specific
path.
.PP
Alternatively, malloc may be avoided entirely by providing a buffer to the
.B nls_search_with_buf
function, which takes a pre-allocated buffer, and a pointer to the size of the
buffer.  If the function returns NULL, it additionally sets the size pointer
to the number of bytes required to store the result, allowing the caller to
potentially realloc and call the function again.
.PP
Language preferences are taken from the environment variables
.BR LANGUAGE ,
.BR LC_ALL ,
.B LC_MESSAGES
and
.BR LANG .
The function
.B nls_search_get_prefs
will return an allocated opaque object which encodes the preferences derived
from those environment variables. This object should be released with the
.B nls_search_free_prefs
function.  Refer to the gettext documentation for more details as to how these
environment variables are interpreted.
.SH EXAMPLE
.nf
\f(CW
struct nls_search_prefs *prefs = nls_search_get_prefs();
char *dir_n = nls_search_find_file("/usr/share/info/%s/dir", prefs);
char *dir = dir_n ? dir_n : "/usr/share/info/dir";
// do something with dir
free(dir_n);
nls_search_free_prefs(prefs);
.SH COPYRIGHT
Copyright \(co 2014, 2015 Brendan O'Dea <bod@debian.org>
.br
All rights reserved.
.PP
Use of this source code is governed by a BSD-style license that can be
found in the COPYING file or at http://opensource.org/licenses/BSD-3-Clause
.SH SEE ALSO
The gettext documentation on setting locales via envionment variables at
https://www.gnu.org/software/gettext/manual/html_node/Setting-the-POSIX-Locale.html
