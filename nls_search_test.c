/*
 * Test harness for nls_search.
 *
 * Copyright (c) 2014, 2015 Brendan O'Dea <bod@debian.org>
 * All rights reserved.
 *
 * Use of this source code is governed by a BSD-style license that can be
 * found in the COPYING file or at http://opensource.org/licenses/BSD-3-Clause
 *
 * Options are key=value pairs.
 *
 *   max_malloc=bytes
 *       malloc will return NULL for larger allocations
 *
 *   locale=str
 *       setlocale will return this value for LC_MESSAGES
 *
 *   language=str
 *       getenv will return this value for LANGUAGE
 *
 *   file=path
 *   directory=path
 *       stat will return fake values for these paths of the appropriate type
 *
 *   type=any (default)
 *   type=file
 *   type=directory
 *       search for any path, or specifically files or directories
 *
 *   buffer_size=size
 *       pass buffer of the given size
 *
 * Any other arguments are passed as the template parameter to nls_search.
 *
 * Possible output lines:
 *
 *   malloc_failed=SIZE
 *       malloc called for a value (SIZE) greater than set by max_malloc
 *
 *   get_prefs_failed
 *       unable to set locale preferences
 *
 *   buffer_size_required=PATH
 *       buffer set with buffer_size is too small, SIZE required
 *
 *   match=PATH
 *   no_match
 *       found PATH, or no match was found
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <locale.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "nls_search.h"

/* Malloc/strdup will fail for allocations greater than this value. */
static size_t _max_malloc = 4096;
static void *_fake_malloc(size_t size) {
  if (size > _max_malloc) return NULL;
  return malloc(size);
}

static char *_fake_strdup(const char *str) {
  if (strlen(str) > _max_malloc) return NULL;
  return strdup(str);
}

/* Return value for setlocale(LC_MESSAGES, ""). */
static char *_locale = NULL;
static char *_fake_setlocale(int category, const char *locale) {
  if (category == LC_MESSAGES && locale && !*locale) return _locale;
  return setlocale(category, locale);
}

/* Return value for getenv("LANGUAGE"). */
static char *_language = NULL;
static char *_fake_getenv(const char *name) {
  if (!strcmp(name, "LANGUAGE")) return _language;
  return getenv(name);
}

/* Files and/or directories which "exist" on disk. */
static char *_files[64] = { NULL };
static char *_directories[64] = { NULL };
static int _nfiles = 0;
static int _ndirectories = 0;
static int _fake_stat(const char *path, struct stat *buf) {
  int i;
  for (i = 0; i < _nfiles; i++) {
    if (!strcmp(path, _files[i])) return stat("/etc/passwd", buf);
  }
  for (i = 0; i < _ndirectories; i++) {
    if (!strcmp(path, _directories[i])) return stat("/etc", buf);
  }
  errno = ENOENT;
  return -1;
}

/* Malloc error handler. */
static int _malloc_failed = 0;
static void _memory_error_handler(size_t size,
                                  const char *file __attribute__ ((unused)),
                                  int line __attribute__ ((unused))) {
  if (!_malloc_failed) _malloc_failed = size;
}

/* Overridable system/library calls. */
extern void *(*nls_search_malloc_)(size_t size);
extern char *(*nls_search_strdup_)(const char *str);
extern char *(*nls_search_setlocale_)(int category, const char *locale);
extern char *(*nls_search_getenv_)(const char *name);
extern int (*nls_search_stat_)(const char *path, struct stat *buf);

static enum nls_search_type _type = NLS_SEARCH_ANY;
static size_t _buffer_size = 0;

void parse_keyword(char *keyword, char *value) {
  if (!strcmp(keyword, "max_malloc")) {
    _max_malloc = atoi(value);
  } else if (!strcmp(keyword, "locale")) {
    _locale = value;
  } else if (!strcmp(keyword, "language")) {
    _language = value;
  } else if (!strcmp(keyword, "file")) {
    int max_files = sizeof(_files) / sizeof(_files[0]);
    if (_nfiles >= max_files) {
      fprintf(stderr, "too many fake files: max=%d\n", max_files);
      exit(2);
    }
    _files[_nfiles++] = value;
  } else if (!strcmp(keyword, "directory")) {
    int max_directories = sizeof(_directories) / sizeof(_directories[0]);
    if (_ndirectories >= max_directories) {
      fprintf(stderr, "too many fake directories: max=%d\n", max_directories);
      exit(2);
    }
    _directories[_ndirectories++] = value;
  } else if (!strcmp(keyword, "type")) {
    if (!strcmp(value, "any")) {
      _type = NLS_SEARCH_ANY;
    } else if (!strcmp(value, "file")) {
      _type = NLS_SEARCH_FILE;
    } else if (!strcmp(value, "directory")) {
      _type = NLS_SEARCH_DIRECTORY;
    } else {
      fprintf(stderr, "invalid type %s\n", value);
      exit(2);
    }
  } else if (!strcmp(keyword, "buffer_size")) {
    _buffer_size = atoi(value);
  } else {
    fprintf(stderr, "invalid option %s=%s\n", keyword, value);
    exit(2);
  }
}

void run_test(const char *template) {
  struct nls_search_prefs *prefs = nls_search_get_prefs();
  char *match;
  char *buf = NULL;
  size_t size = 0;
  if (_buffer_size) {
    size = _buffer_size;
    buf = malloc(size);
    if (!buf) {
      fprintf(stderr, "can't alloc buffer of %d bytes\n", size);
      exit(1);
    }
  }

  if (!prefs) {
    if (_malloc_failed) {
      printf("malloc_failed=%d\n", _malloc_failed);
    }
    printf("get_prefs_failed\n");
  }

  _malloc_failed = 0;
  if ((match = nls_search_with_buf(template, prefs, _type, buf, &size))) {
    printf("match=%s\n", match);
  } else if (buf && size > _buffer_size) {
    printf("buffer_size_required=%d\n", size);
  } else if (_malloc_failed) {
    printf("malloc_failed=%d\n", _malloc_failed);
  } else {
    printf("no_match\n");
  }      

  if (buf) {
    free(buf);
  } else {
    free(match);
  }

  nls_search_free_prefs(prefs);
}

int main(int argc, char *argv[]) {
  /* Setup fake system/library calls. */
  nls_search_malloc_ = _fake_malloc;
  nls_search_strdup_ = _fake_strdup;
  nls_search_setlocale_ = _fake_setlocale;
  nls_search_getenv_ = _fake_getenv;
  nls_search_stat_ = _fake_stat;
  
  /* Setup OOM handler. */
  nls_search_memory_error_handler = _memory_error_handler;

  /* Parse command line. */
  while (--argc) {
    char *arg = *++argv;
    char *value = strchr(arg, '=');
    if (value) {
      *value++ = '\0';
      parse_keyword(arg, value);
    } else {
      run_test(arg);
    }
  }
  return 0;
}
