# Makefile for nls-search.
#
# Copyright (c) 2014, 2015 Brendan O'Dea <bod@debian.org>
# All rights reserved.
#
# Use of this source code is governed by a BSD-style license that can be
# found in the COPYING file or at http://opensource.org/licenses/BSD-3-Clause

DESTDIR =
prefix = /usr/local
exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin
libdir = $(exec_prefix)/lib
includedir = $(prefix)/include
datarootdir = $(prefix)/share
mandir = $(datarootdir)/man
man3dir = $(mandir)/man3

LIB_NAME = libnls-search
LIB_VERSION = 1.0
SO_VERSION = 1

HEADER = nls_search.h
MANUAL = nls_search.3
LIBRARY_SHARED = $(LIB_NAME).so
LIBRARY_FULL = $(LIBRARY_SHARED).$(LIB_VERSION)
LIBRARY_SONAME = $(LIBRARY_SHARED).$(SO_VERSION)

INSTALL = /usr/bin/install
INSTALL_PROGRAM = $(INSTALL)
INSTALL_DATA = $(INSTALL) -m 644

GZIP = /bin/gzip

CFLAGS = -g -W -Wall
LDFLAGS = -fPIC
TESTFLAGS = --verbose

library: $(LIBRARY_FULL)

$(LIBRARY_FULL): nls_search.o
	$(CC) $(CFLAGS) $(LDFLAGS) $? -shared -Wl,-soname=$(LIBRARY_SONAME) -o $@
	ln -sf $(LIBRARY_FULL) $(LIBRARY_SONAME)
	ln -sf $(LIBRARY_SONAME) $(LIBRARY_SHARED)

nls_search_test: nls_search_test.o $(LIBRARY_FULL)
	LD_RUN_PATH=$(CURDIR) \
	$(CC) $(CFLAGS) $(LDFLAGS) nls_search_test.o -L. -lnls-search -o $@

nls_search_test.o: nls_search_test.c $(HEADER)
nls_search.o: nls_search.c $(HEADER)

install: library
	$(INSTALL) -d $(DESTDIR)$(includedir)
	$(INSTALL_DATA) $(HEADER) $(DESTDIR)$(includedir)
	$(INSTALL) -d $(DESTDIR)$(libdir)
	$(INSTALL_PROGRAM) $(LIBRARY_FULL) $(DESTDIR)$(libdir)
	ln -sf $(LIBRARY_FULL) $(DESTDIR)$(libdir)/$(LIBRARY_SONAME)
	ln -sf $(LIBRARY_SONAME) $(DESTDIR)$(libdir)/$(LIBRARY_SHARED)
	$(INSTALL) -d $(DESTDIR)$(man3dir)
	$(INSTALL_DATA) $(MANUAL) $(DESTDIR)$(man3dir)
	$(GZIP) -f9 $(DESTDIR)$(man3dir)/$(MANUAL)
	for page in `awk \
	    '/^\./ { p = 0 } p > 0 \
	     { print } /^\.SH NAME$$/ \
	     { p = 1 }' $(MANUAL) | \
	    tr '\n' ' ' | \
	    sed 's/\\\\-.*//' |  tr , ' '`; \
	do \
	  if [ -n "$$page" ] && [ "$$page" != nls_search ]; then \
	    ln -sf $(MANUAL).gz $(DESTDIR)$(man3dir)/$$page.3.gz; \
	  fi \
	done

check test: nls_search_test
	run-tests $(TESTFLAGS) tests

valgrind: nls_search_test
	run-tests $(TESTFLAGS) --valgrind tests

clean realclean distclean:
	$(RM) *.o $(LIBRARY_SHARED) $(LIBRARY_FULL) $(LIBRARY_SONAME) nls_search_test

.PHONY: library install check test valgrind clean realclean distclean
