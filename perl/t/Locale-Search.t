#!/usr/bin/perl

# Locale::Search tests

BEGIN { print "1..13\n" }

use blib;
use Locale::Search;

# Module loaded.
print "ok 1\n";

# Clear environment.
delete @ENV{qw/LANGUAGE LC_ALL LC_MESSAGES LANG/};

# Valid full locale: en_AU.UTF-8.
$ENV{LANG} = 'en_AU.UTF-8';
if (my $search = Locale::Search->new) {
  print "ok 2\n";
  if ($search->find("t/test/%s/file") eq "t/test/en_AU.UTF-8/file") {
    print "ok 3\n";
  } else {
    print "not ok 3\n";
  }
} else {
  print "not ok 2\n";
  print "ok 3 # skip - invalid locale: en_AU.UTF-8\n";
}

# Invalid full locale: en_AU.ISO-8859-1 (invalid encoding).
$ENV{LANG} = 'en_AU.ISO-8859-1';
if (my $search = Locale::Search->new) {
  print "not ok 4\n";
} else {
  print "ok 4\n";
}

# Valid locale: en_US (no encoding).
$ENV{LANG} = 'en_US.UTF-8';
if (my $search = Locale::Search->new) {
  print "ok 5\n";
  if ($search->find("t/test/%s/file") eq "t/test/en_US/file") {
    print "ok 6\n";
  } else {
    print "not ok 6\n";
  }
} else {
  print "not ok 5\n";
  print "ok 6 # skip - invalid locale: en_US.UTF-8\n";
}

# Valid locale: fr (no territory).
$ENV{LANG} = 'fr_FR.UTF-8';
if (my $search = Locale::Search->new) {
  print "ok 7\n";
  if ($search->find("t/test/%s/file") eq "t/test/fr/file") {
    print "ok 8\n";
  } else {
    print "not ok 8\n";
  }
} else {
  print "not ok 7\n";
  print "ok 8 # skip - invalid locale: fr_FR.UTF-8\n";
}

# File/directory specific tests.
$ENV{LANG} = 'en_AU.UTF-8';
if (my $search = Locale::Search->new) {
  print "ok 9\n";
  if ($search->find_file("t/test/%s/file") eq "t/test/en_AU.UTF-8/file") {
    print "ok 10\n";
  } else {
    print "not ok 10\n";
  }
  if (!defined($search->find_directory("t/test/%s/file"))) {
    print "ok 11\n";
  } else {
    print "not ok 11\n";
  }
  if (!defined($search->find_file("t/test/%s"))) {
    print "ok 12\n";
  } else {
    print "not ok 12\n";
  }
  if ($search->find_directory("t/test/%s") eq "t/test/en_AU.UTF-8") {
    print "ok 13\n";
  } else {
    print "not ok 13\n";
  }
} else {
  print "not ok 9\n";
  for (10..13) {
    print "ok $_ # skip - invalid locale: en_AU.UTF-8\n";
  }
}

