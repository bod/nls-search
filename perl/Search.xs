#define PERL_NO_GET_CONTEXT
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "../nls_search.h"

XS(XS_Locale_Search__constant)
{
  dXSARGS;
  ST(0) = newSViv(CvXSUBANY(cv).any_iv);
  sv_2mortal(ST(0));
  XSRETURN(1);
}

#define CONSTANT(_sub_, _enum_) \
  do { \
    CV *cv = newXSproto(_sub_, XS_Locale_Search__constant, file, ""); \
    CvXSUBANY(cv).any_iv = _enum_; \
  } while (0)

MODULE = Locale::Search  PACKAGE = Locale::Search

BOOT:
 {
  CONSTANT("Locale::Search::Any",	NLS_SEARCH_ANY);
  CONSTANT("Locale::Search::File",	NLS_SEARCH_FILE);
  CONSTANT("Locale::Search::Directory",	NLS_SEARCH_DIRECTORY);
 }

struct nls_search_prefs *
new(class)
  const char *class

 PROTOTYPE: $
 CODE:
  RETVAL = nls_search_get_prefs();

 OUTPUT:
  RETVAL

void
DESTROY(prefs)
  struct nls_search_prefs *prefs

 CODE:
  nls_search_free_prefs(prefs);

void
find(self, template, type = NLS_SEARCH_ANY)
  struct nls_search_prefs *self
  const char *template
  unsigned char type

 PROTOTYPE: $$;$
 PPCODE:
  char buf[4096];
  size_t size = sizeof(buf);
  if (!nls_search_with_buf(template, self, type, buf, &size)) {
    XSRETURN_UNDEF;
  }

  ST(0) = newSVpv(buf, 0);
  sv_2mortal(ST(0));
  XSRETURN(1);
