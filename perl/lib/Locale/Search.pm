package Locale::Search;

use 5.020002;
use strict;
use warnings;

our $VERSION = '0.01';

require XSLoader;
XSLoader::load('Locale::Search', $VERSION);

sub find_file {
  my ($self, $template) = @_;
  return $self->find($template, &Locale::Search::File());
}

sub find_directory {
  my ($self, $template) = @_;
  return $self->find($template, &Locale::Search::Directory());
}

*find_dir = \&find_directory;

1;

__END__
=head1 NAME

Locale::Search - Perl extension to locate locale-specific files

=head1 SYNOPSIS

  use Locale::Search;
  my $nls_search = Locale::Search->new;
  my $dir = $nls_search->find_file('/usr/share/info/%s/dir') ||
      '/usr/share/info/dir';

=head1 DESCRIPTION

Find the path which best matches the user's language preferences.

=over 4

=item new()

Constructor.  Parses the locale-specific environment variables C<LANGUAGE>,
C<LC_ALL>, C<LC_MESSAGES> and C<LANG>.  Refer to the C<gettext> documentation
for more details as to how these are interpreted.

=item find(I<template>, [I<type>])

Search for the first path which matches the I<template> for the given language
preferences requested and and optionally I<type>, which may be one of:

=over 4

=item C<Locale::Search::Any>

=item C<Locale::Search::File>

=item C<Locale::Search::Directory>

Type of path to match: file, directory or any (the default).

=back

The I<template> should contain a C<%s> token, which will be replaced by a
string for the first matching language.  If the template doesn't include a
token, then a trailing C<%s> is implied.

The first matching path is returned, or C<undef> if none was found.

=item find_file(I<template>)

Convenience method, equivalent to:

  $nls_search->find($template, Locale::Search::File)

=item find_directory(I<template>)

=item find_dir(I<template>)

Convenience method, equivalent to:

  $nls_search->find($template, Locale::Search::Directory)

=back

=head1 SEE ALSO

I<Locale Environment Variables> in the C<gettext> info pages.

=head1 AUTHOR

Brendan O'Dea E<lt>bod@debian.orgE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2015 by Brendan O'Dea

Use of this source code is governed by a BSD-style license that can be found
in the COPYING file or at L<http://opensource.org/licenses/BSD-3-Clause>

=cut
